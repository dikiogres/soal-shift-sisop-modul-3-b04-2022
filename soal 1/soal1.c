#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <pwd.h>
#include <pthread.h>

void exec(char *cmd, char *argv[]){
    pid_t child_id;
    child_id = fork();
    int cek;

    if (child_id == 0){
        execv(cmd, argv);
    }

    else{
        while (wait(&cek) > 0)
            ;
    }
}


void *unzip_quote(){
    char *argv[] = {"unzip", "quote.zip", "-d", "/home/arc/Downloads/test/quote", NULL};
    exec("/bin/unzip", argv);
}

void *unzip_music(){
    char *argv[] = {"unzip", "music.zip", "-d", "/home/arc/Downloads/test/music", NULL};
    exec("/bin/unzip", argv);
}

void decode(char *dir, char *namafile){
    chdir(dir);
    char *argv[] = {"base64", "-d", namafile, NULL};
    exec("/bin/base64", argv);
}

void *mv_quote(){
    char *argv[] = {"find", "/home/arc/Downloads/test/", "-name", "quote.txt", "-exec", "mv", "-t", "/home/arc/Downloads/test/hasil", "{}", "+", NULL};
    exec("/bin/find", argv);
}

void *mv_music(){
    char *argv[] = {"find", "/home/arc/Downloads/test/", "-name", "music.txt", "-exec", "mv", "-t", "/home/arc/Downloads/test/hasil", "{}", "+", NULL};
    exec("/bin/find", argv);
}

void* mk_hasil(){
    char *argv[] = {"mkdir", "hasil", NULL};
    exec("/bin/mkdir", argv);
}

void* zip_hasil1(){
    char *argv[] = {"zip","-P", "mihinomenestsatrio", "-r", "hasil.zip", "hasil", NULL};
    exec("/bin/zip", argv);
}

void *unzip_hasil(){
    char *argv[] = {"unzip", "hasil.zip", NULL};
    exec("/bin/unzip", argv);
}

void* zip_hasil2(){
    char *argv[] = {"zip","-P", "mihinomenestsatrio", "-r", "hasil.zip", "hasil", "no.txt", NULL};
    exec("/bin/zip", argv);
}

void addno(){
    FILE *fptr = fopen("no.txt", "w");
    fprintf(fptr, "no");
}

void* touch(char *dest, char *name){
    chdir(dest);
    char *argv[] = {"touch", name, NULL};
    exec("/bin/touch", argv);
}

void* newline(){

    char *argv[] = {"echo", "\n", NULL};
    exec("/bin/echo", argv);
}

void* decodequote(){
    freopen("quote.txt", "w", stdout);
    decode("/home/arc/Downloads/test/quote", "q1.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q2.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q3.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q4.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q5.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q6.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q7.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q8.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q9.txt");
    fclose(stdout);
}

void* decodemusic(){
    freopen("music.txt", "w", stdout);
    decode("/home/arc/Downloads/test/music", "m1.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m2.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m3.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m4.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m5.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m6.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m7.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m8.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m9.txt");
    fclose(stdout);
}


int main(){
    pthread_t n1, n2, n3, n4;
    mk_hasil();
    pthread_create(&n1, NULL, &unzip_music, NULL);
    pthread_create(&n2, NULL, &unzip_quote, NULL);
    pthread_join(n1, NULL);
    pthread_join(n2, NULL);
    decodemusic();
    decodequote();
    pthread_create(&n3, NULL, &mv_music, NULL);
    pthread_create(&n4, NULL, &mv_quote, NULL);
    pthread_join(n3, NULL);
    pthread_join(n4, NULL);
    zip_hasil1();
    unzip_hasil();
    touch("/home/Sisop3/", "no.txt");
    addno();
    zip_hasil2();
}
