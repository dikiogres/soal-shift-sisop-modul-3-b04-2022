# Kelompok B04 Sistem Operasi 2022

### Anggota

| Nama                               | NRP        |
|------------------------------------|------------|
| Lia Kharisma Putri                 | 5025201034 |
| Muhammad Dzikri Fakhrizal Syairozi | 5025201201 |
| Mohamad Kholid Bughowi             | 5025201253 |
<br/>

### Soal 1
Novak yang berselancar di internet menemukan file yang tulisannya tidak jelas dari situs yang tidak memiliki pengguna. Diminta untuk membuat program untuk memecahkan kode-kode di dalam file. yang disimpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.
#### Poin A
Pada poin A diminta untuk mendownload dua file zip yaitu quote.zip dan music.zip serta unzip kedua file zip tersebut dengan menggunakan thread di dua folder yang berbeda.
```c
void exec(char *cmd, char *argv[]){
   pid_t child_id;
   child_id = fork();
   int cek;
 
   if (child_id == 0){
       execv(cmd, argv);
   }
 
   else{
       while (wait(&cek) > 0)
           ;
   }
}
```
- menggunakan fungsi exec  untuk membuat fork sekalian dengan parent nya, sehingga hanya perlu memanggil fungsi ini untuk menggunakan execv tanpa harus membuat fork satu persatu.
```c
void *unzip_quote(){
    char *argv[] = {"unzip", "quote.zip", "-d", "/home/arc/Downloads/test/quote", NULL};
    exec("/bin/unzip", argv);
}

void *unzip_music(){
    char *argv[] = {"unzip", "music.zip", "-d", "/home/arc/Downloads/test/music", NULL};
    exec("/bin/unzip", argv);
}
```
- di atas adalah fungsi untuk meng-unzip quote dan music  menggunakan execv. -d digunakan untuk meng-unzipnya ke direktori quote dan music yang berada di direktori Sisop3.
#### Poin B
Pada poin B diminta untuk men-decode semua file.txt yang ada dengan base 64 dan masukkan hasilnya dalam satu
file.txt yang baru untuk masing-masing folder
```c
void decode(char *dir, char *namafile){
    chdir(dir);
    char *argv[] = {"base64", "-d", namafile, NULL};
    exec("/bin/base64", argv);
}
```
- di atas adalah fungsi decode yang digunakan untuk men-decode semua file txt yang diminta. 
```c
void* newline(){

    char *argv[] = {"echo", "\n", NULL};
    exec("/bin/echo", argv);
}

void* decodequote(){
    freopen("quote.txt", "w", stdout);
    decode("/home/arc/Downloads/test/quote", "q1.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q2.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q3.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q4.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q5.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q6.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q7.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q8.txt");
    newline();
    decode("/home/arc/Downloads/test/quote", "q9.txt");
    fclose(stdout);
}

void* decodemusic(){
    freopen("music.txt", "w", stdout);
    decode("/home/arc/Downloads/test/music", "m1.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m2.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m3.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m4.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m5.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m6.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m7.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m8.txt");
    newline();
    decode("/home/arc/Downloads/test/music", "m9.txt");
    fclose(stdout);
}
```
- fungsi diatas digunakan untuk meng-decode file-file txt yang berada di direktori quote dan music.
- menggunakan fungsi newline untuk memberikan enter di setiap baris output nya.
- Command freopen untuk mengeprint seluruh teks yang dihasilkan di terminal ke file yang telah ditentukan.
- fclose digunakan untuk menutupnya setelah seluruh teks di terminal sudah selesai di print.
#### Poin C
Pada poin c diminta untuk memindahkan kedua file.txt yang berisi hasil decoding ke folder yang baru bernama
hasil.
```c
void* mk_hasil(){
    char *argv[] = {"mkdir", "hasil", NULL};
    exec("/bin/mkdir", argv);
}

void *mv_quote(){
    char *argv[] = {"find", "/home/arc/Downloads/test/", "-name", "quote.txt", "-exec", "mv", "-t", "/home/arc/Downloads/test/hasil", "{}", "+", NULL};
    exec("/bin/find", argv);
}

void *mv_music(){
    char *argv[] = {"find", "/home/arc/Downloads/test/", "-name", "music.txt", "-exec", "mv", "-t", "/home/arc/Downloads/test/hasil", "{}", "+", NULL};
    exec("/bin/find", argv);
}
```
- pertama membuat direktori hasil untuk menampung file-file yang ingin dipindahkan.
- menggunakan fungsi mv_quote dan mv_music untuk memindahkan file quote.txt dan music.txt ke direktori hasil.
- menggunakan command find untuk menemukan file yang ingin dipindahkan, dan command -exec mv untuk memindahkan file yang dicari.
#### Poin D 
diminta untuk meng-zip dan folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama
user]'. (contoh password : mihinomenestnovak)
```c
void* zip_hasil1(){
    char *argv[] = {"zip","-P", "mihinomenestsatrio", "-r", "hasil.zip", "hasil", NULL};
    exec("/bin/zip", argv);
}
```
- menggunakan command zip untuk meng-zip file, -P untuk memberikan password ketika folder tersebut di zip dan -r digunakan untuk mengrekursi seluruh file/folder yang berada di direktori hasil.
#### Poin E 
diminta untuk unzip file hasil.zip dan buat file no.txt
dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt
menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
```c
void *unziphasil()
{
    char *argv[] = {"unzip", "hasil.zip", NULL};
    exec("/bin/unzip", argv);
}
void* ziphasil2()
{
    char *argv[] = {"zip","-P", "mihinomenestsatrio", "-r", "hasil.zip", "hasil", "no.txt", NULL};
    exec("/bin/zip", argv);
}
void addno()
{
    FILE *fptr = fopen("no.txt", "w");
    fprintf(fptr, "no");
}
void* touch(char *dest, char *name)
{
    chdir(dest);
    char *argv[] = {"touch", name, NULL};
    exec("/bin/touch", argv);
}
```
- pertama mengunzip direktori hasil.zip dengan function unzip_hasil().
- selanjutnya membuat file bernama no.txt dengan fungsi touch() dan menambahkan teks no menggunakan fungsi addno(). 
- Setelah semua selesai,menggunakan fungsi zip_hasil2() untuk meng-zip ulang file.
#### Fungsi main 
```c
int main(){
    pthread_t n1, n2, n3, n4;
    mk_hasil();
    pthread_create(&n1, NULL, &unzip_music, NULL);
    pthread_create(&n2, NULL, &unzip_quote, NULL);
    pthread_join(n1, NULL);
    pthread_join(n2, NULL);
    decodemusic();
    decodequote();
    pthread_create(&n3, NULL, &mv_music, NULL);
    pthread_create(&n4, NULL, &mv_quote, NULL);
    pthread_join(n3, NULL);
    pthread_join(n4, NULL);
    zip_hasil1();
    unzip_hasil();
    touch("/home/Sisop3/", "no.txt");
    addno();
    zip_hasil2();
}
```
### Soal 2

### Soal 3
#### Poin A
Diminta untuk mengekstrak file zip hartakarun.zip. Untuk proses pengekstrakannya dilakukan secara manual. Sedangkan untuk pengkategorian tiap file dilakukan menggunakan program pada file soal3.c
#### Poin B
Semua file dikategorikan kedalam folder. File yang tidak memiliki ekstensi dimasukkan ke folder Unknown dan untuk file hidden dimasukkan kedalam folder Hidden dengan syntax berikut.
```c
void move_file(char *p_from, char *p_cwd){
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    convert_to_lower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    create_dir(file_ext);
    rename(p_from, p_cwd);
}
```
#### Poin C
Setiap file yang dikategorikan diproses pada satu thread. Pertama, inisiasi pthread. Disini pthread diinisiasi sebanyak 200.
```c
pthread_t tid[200];
```
Kemudian setiap kali program membaca file didalam folder hartakarun dengan dirent, jalankan thread nya.
```c
while((dp = readdir(dir)) != NULL){
    if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
        char fileName[10000];
        sprintf(fileName, "%s/%s", from, dp->d_name);
        strcpy(s_path.from, fileName);

        pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
        sleep(1);
        index++;
    }
}
```
#### Poin D
Pada source code client.c, dilakukan proses kompresi folder hartakarun ke zip. Kemudian file ini dikirimkan ke server. Proses zip dilakukan dengan perintah zip pada linux yang dieksekusi melalui fungsi `execv()` sebagai berikut.
```c
void zipfile(){
    int status;
    if(fork()==0){
        char *argv[]={"zip","-rm","hartakarun.zip","../hartakarun",NULL};
        execv("/bin/zip",argv);
    }else while(wait(&status)>0);
}
```
Kemudian, file hartakarun.zip dikirimkan ke server dengan fungsi `send_file()` seperti berikut.
```c
void send_file(FILE *fp, int sockfd){
  int n;
  char data[SIZE] = {0};
  bzero(data,SIZE);
  int size_file=0;
  while((size_file = fread(data,sizeof(char),SIZE,fp))>0) {
    if (send(sockfd, data, sizeof(data), 0) == -1) {
      perror("[-]Error in sending file.");
      exit(1);
    }
    bzero(data, SIZE);
  }
}
```
Agar bisa menghubungkan ke server, maka perlu dibuat socket.
```c
struct sockaddr_in server_addr;
 
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    perror("[-]Error in socket");
    exit(1);
  }
  printf("[+]Server socket created successfully.\n");
 
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = port;
  server_addr.sin_addr.s_addr = inet_addr(ip);
  char buffer[1024]={0};
  e = connect(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
  if(e == -1) {
    perror("[-]Error in socket");
    exit(1);
  }
 printf("[+]Connected to Server.\n");
```
#### Poin E
Untuk mengirimkan file hartakarun.zip ke server, client perlu mengetikkan perintah `send hartakrun.zip`.
```c
while(jalan){
    // getchar();
   bzero(choice, 1024);
   scanf(" %[^\n]s", choice);
   printf("%s\n",choice);
   write(sockfd, choice, strlen(choice));
   if(!(strcmp(choice,"send hartakarun.zip"))){
        fp = fopen(filename, "r");
        if (fp == NULL) {
                perror("[-]Error in reading file.");
                exit(1);
        }
 
  send_file(fp, sockfd);
  printf("[+]File data sent successfully.\n");
  jalan=0;
  } else{
    printf("hah\n");
  }
  printf("[+]Closing the connection.\n");
  close(sockfd);
}
```
