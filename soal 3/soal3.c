#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>

pthread_t tid[200];
int flag = 1;

typedef struct struct_path{
    char from[PATH_MAX];
    char cwd[PATH_MAX];
} struct_path;

void create_dir(void *param);
void convert_to_lower(char *param);
int check_file(const char *path);
void move_file(char *p_from, char *p_cwd);
void *move_move(void *s_path);
void sort_file(char *from);

int main() {
    struct_path s_path;
    getcwd(s_path.cwd, sizeof(s_path.cwd));

    strcpy(s_path.from, "/home/bughowi/shift3/hartakarun");
    sort_file(s_path.from);

}

// move file
void move_file(char *p_from, char *p_cwd){
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    convert_to_lower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    create_dir(file_ext);
    rename(p_from, p_cwd);
}

// move
void *move_move(void *s_path){
    struct_path s_paths = *(struct_path*) s_path;
    move_file(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}

// sorting file by category
void sort_file(char *from){
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/bughowi/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }
}

void convert_to_lower(char *param){
    int i = 0;
    for (i; param[i]; i++){
        param[i] = tolower(param[i]);
    }
}

void create_dir(void *param){
    char *value = (char *) param;
    char path[PATH_MAX];

    strcpy(path, "/home/bughowi/shift3/hartakarun/");
    strcat(path, value);

    int res = mkdir(path, 0777);
    if(res == -1){}
}

int check_file(const char *path){
    struct stat stat_path;
    stat(path, &stat_path);
    return S_ISREG(stat_path.st_mode);
}
